# Cadavre exquis

![](example.gif)

## Local run

```bash
./local_docker.sh
```

# Launch application in cluster with Docker Swarm
### Required:
- Two or more machines (or virtual machines) preferably in a private network
- Docker installed on all machines:
```
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg lsb-release

sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```
- Java installed on on one of the machines (this will be our **manager** machine):
`apt install default-jre`

### Starting application:
1. Clone project repository or simply download the code:

*On Manager*
```
git clone https://gitlab.com/the_akatsuki/cloud-cadavre-esquis.git
```
2. Create a swarm:

*On Manager*

From your **manager** machine run `sudo docker swarm init`

3. Add other machines as workers to your swarm:

*On Workers*

Use the command displayed in terminal after you created the swarm: `docker swarm join --token ...`

If you lost the command adding a worker, execute `docker swarm join-token worker` to see it again.

4. Build Docker containers (executed from inside the cloned repo; you might need root privileges):

*On Manager*
```
./gradlew jibDockerBuild
```
5. Launch services:

*On Manager*
```
sudo docker stack deploy --compose-file docker-compose.yml cadavre
```
6. Make sure providers are launched first with the following command:

*On Manager*
```
sudo docker service scale cadavre_providers-adj=3 cadavre_providers-subject=3 cadavre_providers-verb=3
```
7. Go to http://ANY_MACHINE_IP:8080

### Add an outside load balancer
A load balancer outside of the Swarm will allow us to connect to containers on port 80 without worrying about the cluster nodes. It is deployed on its own single-node swarm for consistency.

1. Prepare an additional virtual machine/server with Docker and Java installed

*See "Launch application in cluster with Docker Swarm -> Required" section*

The following commands should all be executed on this machine.

2. Create a Docker Swarm:
```
docker swarm init
```
3. Create a folder for nginx configurations:
```
mkdir -p /data/loadbalancer
```
4. Inside the folder create a file default.conf with the following content:
```
server {
        listen 80;
        location / {
                proxy_pass
                http://backend;
        }
}

upstream backend {
        server <Node1-IP>:8080;
        server <Node2-IP>:8080;
        ...
}
```
Replace <Node?-IP> with the private IP addresses of your swarm nodes hosting the application.

5. Publish load balancer container to port 80 with nginx:
Run the following command:
```
docker service create --name loadbalancer --mount type=bind,source=/data/loadbalancer,target=/etc/nginx/conf.d --publish 80:80 nginx
```
6. Go to http://loadbalancer-IP

# Configuration

### Common

| Env Var       | Description            | Default     |
|---------------|------------------------|-------------|
| `INSTANCE_ID` | Define the instance id | Random UUID |
| `APP_ID`      | Application id         | `""`        |

### Dispatcher

| Env Var         | Description                                     | Default |
|-----------------|-------------------------------------------------|---------|
| `PORT`          | Http port to listen to                          | `8080`  |
| `REGISTER_URLS` | Url to call registers on (list splitted by `,`) | N/A     |

### Register

| Env Var                     | Description                        | Default |
|-----------------------------|------------------------------------|---------|
| `PORT`                      | Http port to listen to             | `8080`  |
| `STORAGE`                   | Define storage target (PG, MEMORY) | `PG`    |
| `POSTGRESQL_ADDON_HOST`     | Postgresql host                    | N/A     |
| `POSTGRESQL_ADDON_PORT`     | Postgresql port                    | N/A     |
| `POSTGRESQL_ADDON_DB`       | Postgresql database to use         | N/A     |
| `POSTGRESQL_ADDON_USER`     | User for postgrsql connection      | N/A     |
| `POSTGRESQL_ADDON_PASSWORD` | Password for postgrsql connection  | N/A     |

### Provider

| Env Var           | Description                                                               | Default                     |
|-------------------|---------------------------------------------------------------------------|-----------------------------|
| ` PORT`           | Http port to listen to                                                    | `8080`                      |
| `REGISTER_URLS`   | Url to call registers on (list splitted by `,`)                           | N/A                         | 
| `PROVIDER_TYPE`   | Define the provider type of the instance (`VERB`, `ADJECTIVE`, `SUBJECT`) | Random in possible values   |
| `ADVERTISER_URL`  | Define the url sent to registers when starting                            | `http://<detectedip>:$PORT` |
